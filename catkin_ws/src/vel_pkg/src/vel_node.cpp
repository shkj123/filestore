#include <ros/ros.h>
#include <geometry_msgs/Twist.h>

int main(int argc, char *argv[])
{
    ros::init(argc,argv,"vel_node");
    setlocale(LC_ALL,"");
    ROS_INFO("程序启动");

    ros::NodeHandle nh;
    ros::Publisher pub=nh.advertise<geometry_msgs::Twist>("/cmd_vel",10);

    geometry_msgs::Twist vel_msg;
    vel_msg.linear.x=0;
    vel_msg.linear.y=0;
    vel_msg.linear.z=0;
    vel_msg.angular.x=0;
    vel_msg.angular.y=0;
    vel_msg.angular.z=0.5;

    ros::Rate rate(10);
    while(ros::ok)
    {
        pub.publish(vel_msg);
        rate.sleep();
    }
    return 0;
}
