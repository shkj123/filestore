#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <opencv2/highgui/highgui.hpp>
#include <stdio.h>
#include <opencv2/opencv.hpp>

cv::Mat LoadImage();

cv::Mat LoadImage()
{
    cv::Mat imageA=cv::imread("/home/robot20/code/1.jpeg",1);
    cv::imshow("imagewindow",imageA);
    cv::waitKey(0);
    if(imageA.empty())
    {
        printf("image empty");
    }
    return imageA;
}
int main(int argc, char *argv[])
{
    ros::init(argc,argv,"pub_image_topic_node");
    ros::NodeHandle nh;

    image_transport::ImageTransport it(nh);
    image_transport::Publisher pub=it.advertise("image",1);

    // cv::Mat image=cv::imread("/home/robot20/code/1.jpeg",1);
    // cv::imshow("imagewindow",image);
    // cv::waitKey(0);
    // if(image.empty())
    // {
    //     printf("image empty");
    // }
    cv::Mat image=LoadImage();

    sensor_msgs::ImagePtr msg=cv_bridge::CvImage(std_msgs::Header(),"bgr8",image).toImageMsg();
    ros::Rate rate(5);
    while(ros::ok)
    {
        ROS_INFO("Message_Result\n");
        pub.publish(msg);
        ros::spinOnce();
        rate.sleep();
        LoadImage();
        break;;
    }
    return 0;
}

// const cv::Mat &image


find_package(OpenCV REQUIRED);
add_executable(pub_image_topic_node src/pub_image_topic_node.cpp)
include_directories(${OpenCV_INCLUDE_DIRS})

 target_link_libraries(pub_image_topic_node
   ${OpenCV_LIBS} ${catkin_LIBRARIES}
 )