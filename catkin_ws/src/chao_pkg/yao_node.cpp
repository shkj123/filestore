#include <ros/ros.h>
#include <std_msgs/String.h>

int main(int argc, char *argv[])
{
    //节点关联Ros
    ros::init(argc,argv,"yao_node");
    printf("This is my_2 project!\n");
    //创建管理者对象
    ros::NodeHandle nh;
    //创建发布者以及话题发布内容
    ros::Publisher pub=nh.advertise<std_msgs::String>("Ready02",10);

    //设置刷新频率
    ros::Rate loop_rate(10);
    while(ros::ok)
    {
        printf("hahahah!!!\n");

        std_msgs::String msg;
        msg.data="Result02";
        pub.publish(msg);
        loop_rate.sleep();
    }
    return 0;
}
